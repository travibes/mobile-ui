import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:get_it/get_it.dart';

import './services/order_service.dart';
import './services/user_Service.dart';
import './services/products_service.dart';
import './routes/route_generator.dart';
import './screens/welcome_journey.dart';

void setupLocator() {
  GetIt.I.registerLazySingleton(() => ProductsService());
  GetIt.I.registerLazySingleton(() => UserService());
  GetIt.I.registerLazySingleton(() => OrderService());
}

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Root Farmerapp widget
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'FPO/Farmer mobile app',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.deepOrangeAccent,
      ),
      home: WelcomeJourney(),
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
