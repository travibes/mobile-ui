import 'package:flutter/foundation.dart';

import '../model/order_item_for_order.dart';
import '../model/user.dart';

class NewOrder {
  double orderAmount;
  bool amountPayed;
  User farmer;
  List<OrderItemForOrder> orderItems;

  NewOrder({
    @required this.orderAmount,
    this.amountPayed = false,
    @required this.farmer,
    @required this.orderItems
});

  Map<String, dynamic> toJson() {
    List<Map<String, dynamic>> items = <Map<String, dynamic>> [];
    orderItems.forEach((item) => items.add(item.toJson()));
    return {
      'orderAmount': orderAmount,
      'amountPayed': amountPayed,
      'farmer': farmer.toJson(),
      'orderItems': items
    };
  }

}