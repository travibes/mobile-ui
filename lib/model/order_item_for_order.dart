import 'package:flutter/foundation.dart';

import '../model/product_for_order.dart';

class OrderItemForOrder {

  ProductForOrder product;
  double price;
  int quantity;

  OrderItemForOrder({@required this.product, @required this.price, @required this.quantity});

  Map<String, dynamic> toJson() {
    return {
      'product': product.toJson(),
      'price': price,
      'quantity': quantity
    };
  }

}