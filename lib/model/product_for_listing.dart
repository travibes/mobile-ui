import 'package:equatable/equatable.dart';

class ProductForListing extends Equatable {
  String productId;
  String productName;
  String productType;
  String price;
  String brand;
  String variety;
  String weightType;
  String productWeight;
  String imageUrl;

  ProductForListing(
      {this.productId,
      this.productName,
      this.productType,
      this.price,
      this.brand,
      this.variety,
      this.weightType,
      this.productWeight,
      this.imageUrl});

  @override
  List<Object> get props => [
        productId,
        productName,
        productType,
        price,
        brand,
        variety,
        weightType,
        productWeight,
        productWeight,
        imageUrl
      ];
}
