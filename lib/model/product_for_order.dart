import 'package:flutter/foundation.dart';

class ProductForOrder {
  int id;

  ProductForOrder({@required this.id});

  Map<String, dynamic> toJson() {
    return {
      'id': id
    };
  }
}