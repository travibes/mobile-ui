import '../model/product_for_listing.dart';

class QuantityCounter {
  ProductForListing product;
  int quantity;

  QuantityCounter({this.product, this.quantity,});
}