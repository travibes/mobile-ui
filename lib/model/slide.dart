import 'package:flutter/material.dart';

class Slide {
  final String imageUrl;
  final String title;
  final String description;

  Slide({
    @required this.imageUrl,
    @required this.title,
    @required this.description,
  });
}

final slideList = [
  Slide(
      imageUrl: 'assets/images/agriinput.png',
      title: 'Order Input',
      description:
          'Enable FPO\'s and farmers in bulk buying of Seed, pesticide and other agri inputs'),
  Slide(
      imageUrl: 'assets/images/book-keeping.png',
      title: 'Digital Bookkeeping',
      description:
          'Technology platform for maintaining ledgers, payments, assets and liabilities for FPO\'s'),
  Slide(
      imageUrl: 'assets/images/buyerconnect.png',
      title: 'Market Linkage',
      description:
          'FPO\'s can connect with listed buyers onto the platform and inform them of available markets and prevailing prices'),
  Slide(
      imageUrl: 'assets/images/loan.png',
      title: 'Easy Credit',
      description:
          'Helping FPO\'s in providing hassle free loan/credit to their farmers'),
];
