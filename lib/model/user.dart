import 'package:flutter/foundation.dart';

import '../model/user_address.dart';

class User {
  int id;
  String name;
  String mobile;
  UserAddress address;

  User({this.id, @required this.name, @required this.mobile, @required this.address});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'mobile': mobile,
      'address': address.toJson()
    };
  }

}
