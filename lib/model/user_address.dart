import 'package:flutter/foundation.dart';

class UserAddress {
  String addressType;
  String state;
  String pincode;
  String fullAddress;

  UserAddress(
      {this.addressType = 'FPO', @required this.state, @required this.pincode, @required this.fullAddress});

  Map<String, dynamic> toJson() {
    return {
      'addressType': addressType,
      'state': state,
      'pincode': pincode,
      'fullAddress': fullAddress
    };
  }

}
