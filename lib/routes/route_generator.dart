import 'package:flutter/material.dart';

import '../screens/error_page.dart';
import '../screens/final_order_page.dart';
import '../screens/home_page.dart';
import '../screens/user_information.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    //Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;

    switch (settings.name) {
      case '/home':
        return MaterialPageRoute(
          builder: (_) => HomePage(),
        );
      case '/userInformation':
        return MaterialPageRoute(
          builder: (_) => UserInformation(data: args),
        );
      case '/finalOrderPage':
        return MaterialPageRoute(
          builder: (_) => FinalOrderPage(),
        );
      case '/errorPage':
        return MaterialPageRoute(
          builder: (_) => ErrorPage(),
        );
      default : _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}