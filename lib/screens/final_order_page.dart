import 'package:flutter/material.dart';

class FinalOrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appTitle = 'Order Done';
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(appTitle),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 150.0,
            ),
            Text(
              'Order placed successfully',
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 10.0,
            ),
            Padding(
              padding: EdgeInsets.only(left: 30.0, right: 30.0,),
              child: Text(
                'Our representative will be in touch with you shortly.',
                style: TextStyle(fontSize: 17.0),
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            Container(
              height: 50,
              width: 200,
              //alignment: AlignmentDirectional.bottomCenter,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/home');
                },
                child: Text(
                  'Go Back To home',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                color: Colors.green,
                //splashColor: Colors.lightGreen,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                textColor: Colors.white,
                padding: const EdgeInsets.all(5),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
