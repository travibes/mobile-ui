import 'package:flutter/material.dart';

import 'package:get_it/get_it.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../model/quantity_counter.dart';
import '../widgets/loader.dart';
import '../model/api_response.dart';
import '../model/product_for_listing.dart';
import '../services/products_service.dart';
import '../widgets/product_item.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ProductsService get productService => GetIt.I<ProductsService>();

  APIResponse<List<ProductForListing>> _apiResponse;
  List<QuantityCounter> quantityCounterList = [];

  //APIResponse<List<ProductForListing>> _apiResponse = APIResponse(data: [ProductForListing(productId: 'weffwef', productName: 'sffwf', productType: 'hqdqwdqd', price: '200', brand: 'dupont', weightType: 'efwefwfw', productWeight: '2', imageUrl: 'http://tineye.com/images/widgets/mona.jpg')]);
  bool _isLoading = false;

  //List<ProductForListing> products = [];
  ScrollController _scrollController = ScrollController();
  static const _increamentFix = 7;
  int _currentCounter = 0;

  @override
  void initState() {
    //products = productService.getProductsList();
    //print('INsideinitstate');
    _fetchProducts();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _fetchProducts();
      }
    });
    //print('After initstate');
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _fetchProducts() async {
    setState(() {
      _isLoading = true;
    });

    _apiResponse = await productService.getProductsList(
        _currentCounter, _increamentFix, _apiResponse);
    _currentCounter++;

    setState(() {
      _isLoading = false;
    });
  }

  void showToast() {
    //Remove all quantityCounterList element with 0 quantity
    quantityCounterList.removeWhere((quantityCounterListItem) => quantityCounterListItem.quantity == 0);
    if (quantityCounterList.length == 0) {
      Fluttertoast.showToast(
          msg: "Please Select Products",
          toastLength: Toast.LENGTH_SHORT,
          backgroundColor: Colors.redAccent,
          textColor: Colors.white);
    }
    else {
      Navigator.of(context).pushNamed('/userInformation', arguments: quantityCounterList);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 20.0,
          ),
          Padding(
            padding: EdgeInsets.only(left: 40.0),
            child: Row(
              children: <Widget>[
                Text(
                  'Agri',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                  'Input',
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    color: Colors.white,
                    fontSize: 25,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.white,
              //borderRadius: BorderRadius.only(topLeft: Radius.circular(75.0)),
            ),
            child: _isLoading && _currentCounter == 0
                ? Loader()
                : ListView(
                    primary: false,
                    padding: EdgeInsets.only(
                      left: 15.0,
                      right: 10.0,
                    ),
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 0.0),
                        child: Container(
                          height: MediaQuery.of(context).size.height - 160.0,
                          child: ListView.builder(
                            controller: _scrollController,
                            itemCount: _apiResponse.data.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ProductItem(
                                productId: _apiResponse.data[index].productId,
                                productName: _apiResponse.data[index].productName,
                                price: _apiResponse.data[index].price,
                                brand: _apiResponse.data[index].brand,
                                variety: _apiResponse.data[index].variety,
                                productWeight: _apiResponse.data[index].productWeight,
                                weightType: _apiResponse.data[index].weightType,
                                imageUrl: _apiResponse.data[index].imageUrl,
                                productType: _apiResponse.data[index].productType,
                                valueSetter: (selectedProduct) {
                                  //Callback of ontap in product_item.dart
                                  bool isSelectedProductFound = false;
                                  for (final quantityCounterItem
                                      in quantityCounterList) {
                                    if (quantityCounterItem.product ==
                                        selectedProduct.product) {
                                      quantityCounterItem.quantity =
                                          selectedProduct.quantity;
                                      isSelectedProductFound = true;
                                      break;
                                    }
                                  }
                                  if (isSelectedProductFound == false) {
                                    quantityCounterList.add(selectedProduct);
                                  }
                                },
                              );
                            },
                          ),
                        ),
                      ),
                      _isLoading && _currentCounter != 0 ? Loader() :
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 50,
                        width: 70,
                        //alignment: AlignmentDirectional.bottomCenter,
                        child: RaisedButton(
                          onPressed: showToast,
                          child: Text(
                            'Checkout',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                          color: Colors.green,
                          //splashColor: Colors.lightGreen,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(5),
                        ),
                      ),
                    ],
                  ),
          ),
        ],
      ),
    );
  }
}
