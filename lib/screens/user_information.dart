import 'package:flutter/material.dart';

import '../model/quantity_counter.dart';
import '../widgets/user_information_form.dart';

class UserInformation extends StatelessWidget {

  final List<QuantityCounter> data;

  UserInformation({@required this.data});

  @override
  Widget build(BuildContext context) {
    final appbarTitle = 'Fill Order Details';
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(appbarTitle),
      ),
      body: Container(
        height: 560.0,
      child: Card(
        child: UserInformationForm(data: data,),
        elevation: 10.0,
        margin: EdgeInsets.only(top: 50.0, left: 10.0, right: 10.0,),
      ),
      ),
    );
  }
}
