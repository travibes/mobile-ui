import 'dart:convert';

import 'package:http/http.dart' as http;

import '../model/new_order.dart';
import '../model/api_response.dart';

class OrderService {

  static const ip = '104.154.172.156:8080';
  static const api = '/saveneworder';

  Future<APIResponse<bool>> createOrder(NewOrder order) {
    //print(json.encode(order.toJson()));
    return http.post(Uri.http(ip, api), headers: {'Content-type': 'application/json'}, body: json.encode(order.toJson())).then((data) {
      //print(data.statusCode);
      if (data.statusCode == 200) {
        return APIResponse<bool>(data: true);
      }
      return APIResponse<bool>(
          error: true, errorMessage: 'An error occured');
    }).catchError((_) =>
        APIResponse<bool>(
            error: true, errorMessage: 'An error occured'));
  }

}