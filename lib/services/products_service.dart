import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../model/product_for_listing.dart';
import '../model/api_response.dart';

class ProductsService {
  static var queryParameters = {
    'pageNumber': '1',
    'pageSize': '3',
  };
  // Get product URL
  //static const API = 'localhost:8081';
  //var API = Uri.http('10.0.2.2:8081', '/getProducts', queryParameters);
  static const ip = '104.154.172.156:8080';
  static const api = '/getProducts';

  Future<APIResponse<List<ProductForListing>>> getProductsList(int pageNumber, int pageSize, APIResponse apiResponse) {
    return http.get(Uri.http(ip, api, {
      'pageNumber': pageNumber.toString(),
      'pageSize': pageSize.toString(),
    })).then((data) {
      if (data.statusCode == 200) {
        print(data.body);
        final jsonData = json.decode(data.body);
        print(jsonData);
        var products = <ProductForListing>[];
        if(apiResponse != null) {
          products = apiResponse.data;
        }
        for (var item in jsonData) {
          final product = ProductForListing(
              productId: item['id'].toString(),
              productName: item['name'].toString(),
              productType: item['productType'].toString(),
              price: item['price'].toString(),
              brand: item['brand'].toString(),
              variety: item['variety'].toString(),
              weightType: item['weightType'].toString(),
              productWeight: item['productWeight'].toString(),
              imageUrl: item['imageUrl'].toString());
          products.add(product);
        }
        return APIResponse<List<ProductForListing>>(data: products);
      }
      return APIResponse<List<ProductForListing>>(
          error: true, errorMessage: 'An error occured');
    }).catchError((_) => APIResponse<List<ProductForListing>>(
        error: true, errorMessage: 'An catch occured'));
  }
}
