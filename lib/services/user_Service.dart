import 'package:http/http.dart' as http;
import 'dart:convert';

import '../model/user_address.dart';
import '../model/api_response.dart';
import '../model/user.dart';

class UserService {

  static const ip = '104.154.172.156:8080';
  static const api = '/saveuserdetails';

  Future<APIResponse<User>> createUser(User user) {
    return http.post(Uri.http(ip, api), headers: {'Content-type': 'application/json'}, body: json.encode(user.toJson())).then((data) {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        var user = User(
          id: jsonData['id'],
          name: jsonData['name'],
          mobile: jsonData['mobile'],
          address: UserAddress(state: jsonData['address']['state'], pincode: jsonData['address']['pincode'])
        );
        return APIResponse<User>(data: user);
      }
      return APIResponse<User>(
          error: true, errorMessage: 'An error occured');
    }).catchError((_) =>
        APIResponse<User>(
            error: true, errorMessage: 'An error occured'));
  }

}