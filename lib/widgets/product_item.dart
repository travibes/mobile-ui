import 'package:flutter/material.dart';

import '../model/product_for_listing.dart';
import '../model/quantity_counter.dart';

import 'package:gramline_ui/screens/home_page.dart';

class ProductItem extends StatefulWidget {
  String productId;
  String productName;
  String price;
  String brand;
  String variety;
  String productWeight;
  String weightType;
  String imageUrl;
  String productType;
  final ValueSetter<QuantityCounter> valueSetter;

  ProductItem(
      {this.productId,
      this.productName,
      this.price,
      this.brand,
        this.variety,
      this.productWeight,
      this.weightType,
      this.imageUrl,
      this.productType,
      this.valueSetter});

  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  int _currentQuantityCounter = 0;
  var product = ProductForListing();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 0.0, top: 25.0),
      child: Container(
        child: Row(
          children: <Widget>[
            Container(
              width: 50.0,
              height: 50.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(widget.imageUrl),
                ),
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            Container(
              width: 160.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        widget.productName, // productName
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 17.0,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      SizedBox(
                        width: 2.0,
                      ),
                      Text(
                        '(' + widget.productWeight + widget.weightType + ')',
                        // weight unit
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 17.0,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ],
                  ),
                  Text(widget.variety),
                  Text(widget.brand), // company
                ],
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            Container(
              width: 100.0,
              height: 40.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(17.0),
                color: Theme.of(context).primaryColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      if (_currentQuantityCounter > 0) {
                        setState(() {
                          _currentQuantityCounter--;
                        });

                        widget.valueSetter(QuantityCounter(
                            product: ProductForListing(
                                productId: widget.productId,
                                productName: widget.productName,
                                price: widget.price,
                                brand: widget.brand,
                                productWeight: widget.productWeight,
                                weightType: widget.weightType,
                                imageUrl: widget.imageUrl,
                                productType: widget.productType
                            ),
                            quantity: _currentQuantityCounter));
                      }
                    },
                    child: Container(
                      height: 20.0,
                      width: 20.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        color: Colors.white,
                      ),
                      child: Center(
                        child: Icon(
                          Icons.remove,
                          color: Colors.black,
                          size: 20.0,
                        ),
                      ),
                    ),
                  ),
                  Text(
                    _currentQuantityCounter.toString(), // quantityCounter
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Montserrat',
                      fontSize: 15.0,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        _currentQuantityCounter++;
                      });
                      widget.valueSetter(QuantityCounter(
                          product: ProductForListing(
                              productId: widget.productId,
                              productName: widget.productName,
                              price: widget.price,
                              brand: widget.brand,
                              productWeight: widget.productWeight,
                              weightType: widget.weightType,
                              imageUrl: widget.imageUrl,
                              productType: widget.productType
                          ),
                          quantity: _currentQuantityCounter));
                    },
                    child: Container(
                      height: 25.0,
                      width: 25.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        color: Colors.white,
                      ),
                      child: Center(
                        child: Icon(
                          Icons.add,
                          color: Colors.black,
                          size: 20.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
