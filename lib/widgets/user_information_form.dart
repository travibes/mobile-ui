import 'dart:ffi';

import 'package:flutter/material.dart';

import 'package:get_it/get_it.dart';

import '../model/new_order.dart';
import '../model/order_item_for_order.dart';
import '../model/product_for_order.dart';
import '../services/order_service.dart';
import '../model/quantity_counter.dart';
import '../model/user.dart';
import '../model/user_address.dart';
import '../widgets/loader.dart';
import '../model/api_response.dart';
import '../services/user_Service.dart';

class UserInformationForm extends StatefulWidget {

  final List<QuantityCounter> data;

  UserInformationForm({this.data});

  @override
  _UserInformationFormState createState() => _UserInformationFormState();
}

class _UserInformationFormState extends State<UserInformationForm> {
  final _formKey = GlobalKey<FormState>();

  final _nameTextEditingController = TextEditingController();
  final _mobileTextEditingController = TextEditingController();
  final _addressTextEditingController = TextEditingController();
  final _stateTextEditingController = TextEditingController();
  final _pinTextEditingController = TextEditingController();

  UserService get userService => GetIt.I<UserService>();

  OrderService get orderService => GetIt.I<OrderService>();

  APIResponse<User> _apiResponse = APIResponse();

  APIResponse<bool> _orderApiResponse = APIResponse();

  bool _isButtonDisabled;

  @override
  void initState() {
    _isButtonDisabled = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(20.0),
              child: TextFormField(
                controller: _nameTextEditingController,
                decoration: InputDecoration(
                  hintText: 'Name',
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter your name';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: TextFormField(
                controller: _mobileTextEditingController,
                decoration: InputDecoration(
                  hintText: 'Enter your 10 digit mobile number',
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter your mobile';
                  }
                  else if (value.length < 10 || value.length > 10) {
                    return 'Please enter 10 digits of your mobile';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: TextFormField(
                controller: _addressTextEditingController,
                decoration: InputDecoration(
                  hintText: 'Address',
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter your address';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 150.0,
                    child: TextFormField(
                      controller: _stateTextEditingController,
                      decoration: InputDecoration(
                        hintText: 'State',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter your state';
                        }
                        return null;
                      },
                    ),
                  ),
                  Container(
                    width: 150.0,
                    child: TextFormField(
                      controller: _pinTextEditingController,
                      decoration: InputDecoration(
                        hintText: 'Pincode',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter your pin';
                        }
                        return null;
                      },
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            Container(
              height: 50,
              width: 250,
              //alignment: AlignmentDirectional.bottomCenter,
              child: _buildRaisedButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRaisedButton() {
    return new RaisedButton(
      color: Colors.green,
      textColor: Colors.white,
      child: _isButtonDisabled ? Loader() : Text(
        'Place Order',
        style: TextStyle(fontSize: 18.0),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      onPressed: _isButtonDisabled ? null : ()  async {
        if(_formKey.currentState.validate()) {
          setState(() {
            _isButtonDisabled = true;
          });
          Scaffold.of(context)
              .showSnackBar(SnackBar(content:
          Text('Processing Data')));

          var user = User(name: _nameTextEditingController.text,
              mobile: _mobileTextEditingController.text,
              address: UserAddress(state: _stateTextEditingController.text,
                  pincode: _pinTextEditingController.text,
                  fullAddress: _addressTextEditingController.text));
          _apiResponse = await userService.createUser(user);
          //Call for order process function
          _processOrder(_apiResponse);
          setState(() {
            _isButtonDisabled = false;
          });
          //Alert box implementation
          if(_apiResponse.error == true || _orderApiResponse.error == true) {
            Navigator.of(context).pushNamed('/errorPage');
          } else {
            Navigator.of(context).pushNamed('/finalOrderPage');
          }
        }
      },
    );
  }


  void _processOrder(APIResponse<User> userData) async {
    List<OrderItemForOrder> orderItems = <OrderItemForOrder> [];
    double totalOrderAmount = 0;
    // iterate over data List and build orderItems
    for(var item in widget.data) {
      orderItems.add(OrderItemForOrder(
          product: ProductForOrder(
              id: int.parse(item.product.productId),
          ),
          price: double.parse(item.product.price)*item.quantity,
          quantity: item.quantity));
      totalOrderAmount += double.parse(item.product.price)*item.quantity;
    }

    var order = NewOrder(
        orderAmount: totalOrderAmount,
        farmer: _apiResponse.data,
        orderItems: orderItems
    );

    _orderApiResponse = await orderService.createOrder(order);

  }


}
